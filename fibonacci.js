// to find N fibonacci number, please paste this code into console, then type fib(n). n - number you wanna return

  function fib(n) {
    var a = 1,
      b = 1;
    for (var i = 3; i <= n; i++) {
      var c = a + b;
      a = b;
      b = c;
    }
    return b;
  }

console.log (fib(5)); // 5
console.log (fib(7)); // 13
console.log (fib(12)); // 144
console.log (fib(31)); // 1346269
