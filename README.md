# Hello! My name is Varnaev Alexander
I've totaly finished this four tasks. They are in the same folders as were.
 Mosaic now is a nice picture. 
 Orange square knows how to move left and up, that's just opposite than right and down:) 
 Also in folder "page-from-jpeg" you could find index.html, run it and see working site! 
 In fourth task, to find Fibonacci any Nth number just copy function from fibonacci.js paste it in console and enter fib(N), where N- is Nth number you wanna find!

* P.s. I worked for the first time with this service, I did not find here something that looks like GithubPages (but I believe there must be one), 
I pushed this progect to my GitHub repositori https://github.com/Sibil70/entry-testing
so all links for this tasks will be on GithubPages:

https://sibil70.github.io/entry-testing/broken-page/ 

https://sibil70.github.io/entry-testing/maze/

https://sibil70.github.io/entry-testing/page-from-jpeg/

Write me please some review for this work. My email: alexandervarnaev@gmail.com


# Available tasks
Hi, here are the testing round tasks for Frontend School at T-Systems.

## 1 Broken page
This nice mosaic is totally broken, please fix it.

## 2 Maze
Help the orange square get out of the frightening maze.

Unfortunately, the orange square doesn't know how to move up and left.
You have to teach it how to do that.

## 3 Page from JPEG
We've lost sources of our main page, only one last screenshot was left.

Please help us compose this web-page again.

## 4 Algorithm in JS
Fibonacci has called. Seems he can’t recall his numbers, except for the first two: 1, 1.

Please create .js file, which Fibonacci could copypaste in his browser console and get any Nth number by inputing the N into it.

